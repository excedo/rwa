### INSTALL
0. Install mongodb or run mongodb via docker image. The backend expects to find a unprotected mongodb instance locally on port 27017
  
  If you need to alter the DB connection string -> Modify the constants file in api/constants/db.js -> MONGOOSEURI

1. Run " npm install " in /api -folder
2. Run " npm install " in /ui -folder
3. Start backend in /api with command " npm start "
4. Start development frontend in /ui with command " npm start "
5. Start production frontend in /ui by building the package with " npm run build " and then running " serve -s build "

!! The development build currently proxies requests to the backend via the following route => requests to :3000/api get redirected to :8080/api
!! This doesn't happen in the production build and you need to configure your hosting solution / server to take care of the proxy.

if you have trouble with number 5. -> please try running " npm install serve -g " to install the serve tool globally on your system.

### USAGE
1. Input a city name to look up weather data from.
2. Read weather data from the boxes that appeared. Top is current weather and bottom boxes contain a 5 day forecast split by day.

### NOTES
Backend start on port 8080
Frontend starts on either port 3000 or 5000 depending on development or production build.

