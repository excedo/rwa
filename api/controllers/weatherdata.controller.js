const express = require('express');
const router = express.Router();
const weatherService = require('../services/weatherdata.service');

// routes
router.post('/getweatherandforecast', getWeatherAndForecast);

module.exports = router;

function getWeatherAndForecast(req, res, next) {
  weatherService
    .getWeatherAndForecast(req.body.city)
    .then(weather => {
      res.json(weather);
    })
    .catch(err => next(err));
}

