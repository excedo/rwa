const request = require('request-promise');
const { APIKEY } = require('../constants/weather');

async function currentWeatherData(city) {

  const currentWeather = JSON.parse(await request(
    `http://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=${APIKEY}&units=metric`,
  ));

  const currentForecast = JSON.parse(await request(
    `http://api.openweathermap.org/data/2.5/forecast?q=${city}&APPID=${APIKEY}&units=metric`,
  ));

  currentWeather.forecast5 = currentForecast;

  return currentWeather;
}

module.exports = {
  currentWeatherData,
};
