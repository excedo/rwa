const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  createdDate: { type: Date, default: Date.now },
  dataRetrieved: { type: 'String' },
  coord: {
    lon: { type: 'String' },
    lat: { type: 'String' },
  },
  weather: [
    {
      id: { type: 'String' },
      main: { type: 'String' },
      description: { type: 'String' },
      icon: { type: 'String' },
    },
  ],
  base: { type: 'String' },
  main: {
    temp: { type: 'String' },
    pressure: { type: 'String' },
    humidity: { type: 'String' },
    temp_min: { type: 'String' },
    temp_max: { type: 'String' },
  },
  visibility: { type: 'String' },
  wind: {
    speed: { type: 'String' },
    deg: { type: 'String' },
  },
  clouds: {
    all: { type: 'String' },
  },
  dt: { type: 'String' },
  sys: {
    type: { type: 'String' },
    id: { type: 'String' },
    message: { type: 'String' },
    country: { type: 'String' },
    sunrise: { type: 'String' },
    sunset: { type: 'String' },
  },
  timezone: { type: 'String' },
  id: { type: 'String' },
  name: { type: 'String' },
  cod: { type: 'String' },
  forecast5 : {
    cod: { type: 'String' },
    message:{ type: 'String' },
    cnt: { type: 'String' },
    list: [
        {
            dt: { type: 'String' },
            main: {
                temp: { type: 'String' },
                temp_min: { type: 'String' },
                temp_max: { type: 'String' },
                pressure: { type: 'String' },
                sea_level: { type: 'String' },
                grnd_level: { type: 'String' },
                humidity: { type: 'String' },
                temp_kf: { type: 'String' },
            },
            weather: [
                {
                    id: { type: 'String' },
                    main: { type: 'String' },
                    description: { type: 'String' },
                    icon: { type: 'String' },
                }
            ],
            clouds: {
                all: { type: 'String' },
            },
            wind: {
                speed: { type: 'String' },
                deg: { type: 'String' },
            },
            sys: {
                pod: { type: 'String' },
            },
            dt_txt: { type: 'String' },
        }
    ],
    city: {
        id: { type: 'String' },
        name: { type: 'String' },
        coord: {
            lat: { type: 'String' },
            lon: { type: 'String' },
        },
        country: { type: 'String' },
        population: { type: 'String' },
        timezone: { type: 'String' },
        sunrise: { type: 'String' },
        sunset: { type: 'String' },
    }
}
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Weatherdata', schema);
