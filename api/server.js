﻿const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandler = require('./utils/error-handler');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Routes
app.use('/api', require('./controllers/weatherdata.controller'));

// Global error handles
app.use(errorHandler);

// Start
app.listen(8080, function() {
  // eslint-disable-next-line no-console
  console.log('Server listening on port ' + 8080);
});
