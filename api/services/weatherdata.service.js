const db = require('../utils/db');
const WeatherData = db.WeatherData;
const {
  currentWeatherData,
} = require('../external-requests/weather');
const { CACHETIMERMILLISECONDS } = require('../constants/weather');

module.exports = {
  getWeatherAndForecast,
};

async function getWeatherAndForecast(city) {
  if (typeof city !== 'undefined') {
    const upperCity = city.charAt(0).toUpperCase() + city.slice(1);
    const weatherData = await WeatherData.find({ name: upperCity })
      .sort({ _id: -1 })
      .limit(1);
    if (weatherData.length === 0) {
      const result = await updateCache(city);
      return [result];
    } else {
      const currentTime = Math.floor(Date.now());
      const weatherTime = Math.floor(
        weatherData[0].createdDate.getTime(),
      );
      if (currentTime - weatherTime > CACHETIMERMILLISECONDS) {
        const result = await updateCache(city);
        return [result];
      } else {
        return weatherData;
      }
    }
  } else {
    throw 'Missing city!';
  }
}

async function updateCache(city) {
  console.log('cache update!');
  const newWeather = await currentWeatherData(city);
  const weatherData = new WeatherData(newWeather);
  weatherData.save();
  return weatherData;
}
