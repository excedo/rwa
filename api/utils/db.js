const mongoose = require('mongoose');
const { MONGOOSEURI } = require('../constants/db');
mongoose.connect(MONGOOSEURI, {
  useCreateIndex: true,
  useNewUrlParser: true,
});
mongoose.Promise = global.Promise;

module.exports = {
  WeatherData: require('../models/weatherdata.model'),
};