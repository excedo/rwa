module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    jest: true,
  },
  extends: 'airbnb',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
    allowImportExportEverywhere: true,
  },
  plugins: ['react'],
  rules: {
    'linebreak-style': 0,
    'no-confusing-arrow': 'off',
    'no-underscore-dangle': 'off',
    'react/jsx-filename-extension' : 0,
    'jsx-quotes': ['warning', 'prefer-single'],
  },
};
