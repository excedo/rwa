import React, { useState } from "react";
import "./App.sass";
import { ContentWrapper, ContentRow, HalfContentRow } from "./components";
import { getWeatherAndForecast } from "./api/weatherapi";
import Logo from "./img/logo.png";

const App = () => {
  const [activeCity, setActiveCity] = useState("");
  const [activeWeatherData, setActiveWeatherData] = useState([]);
  const [errorState, setErrorState] = useState("");

  const ForecastDaySorter = (forecastdays) => {
    const daysObject = {};
    forecastdays.forEach((day) => {
      const date = [day.dt_txt.split(" ")[0]];
      !daysObject[[date]] && (daysObject[[date]] = []);
      daysObject[[date]].push(day);
    });
    return daysObject;
  };

  const HandleInput = (e) => {
    setActiveCity(e.target.value);
  };

  const requestWeatherData = (city) => {
    getWeatherAndForecast(city).then((response) => {
      if (response.status === 200) {
        setErrorState("");
        setActiveWeatherData(response.data[0]);
      }
      if (response.status === 500) {
        setErrorState("Kaupunkia ei löytynyt.");
      }
    });
  };

  return (
    <>
      <ContentWrapper>
        <ContentRow>
          <img src={Logo} alt="Rento" />
        </ContentRow>
        <ContentRow>
          <div className="field">
            <label>Kaupunki</label>
            <div className="control">
              <input
                className="input is-medium"
                type="text"
                placeholder="kaupunki..."
                value={activeCity}
                onChange={(e) => HandleInput(e)}
              />
            </div>
          </div>
        </ContentRow>
        <ContentRow>
          <div className="field">
            <div class="container is-fluid">
              <button
                className="button"
                onClick={() => {
                  requestWeatherData(activeCity);
                }}
              >
                Hae
              </button>
            </div>
          </div>
        </ContentRow>
        {errorState.length > 0 && (
          <ContentRow>
            <div class="notification is-danger">{errorState}</div>
          </ContentRow>
        )}
        {activeWeatherData && activeWeatherData.name ? (
          <>
            <ContentRow>
              <h1 className="title">{activeWeatherData.name}</h1>
              <hr />
            </ContentRow>
            <ContentRow>
              <div className="box">
                <div class="content">
                  <p>
                    <strong>Sää nyt </strong>
                    <br />
                    Lämpötila: {activeWeatherData.main.temp} °C
                    <br />
                    Kosteus: {activeWeatherData.main.humidity} %
                    <br />
                    Ilmanpaine: {activeWeatherData.main.pressure} hPa
                  </p>
                </div>
              </div>
            </ContentRow>
          </>
        ) : null}

        {activeWeatherData && activeWeatherData.name
          ? Object.values(
              ForecastDaySorter(activeWeatherData.forecast5.list)
            ).map((days) => (
              <HalfContentRow>
                <div className="box">
                  <div class="content">
                    <strong>{DateFormat(days[0].dt_txt).split(" ")[0]}</strong>
                    <hr />
                    {days.map((day) => (
                      <p>
                        <strong>{DateFormat(day.dt_txt).split(" ")[1]}</strong>
                        <br />
                        Lämpötila: {day.main.temp} °C
                        <br />
                        Kosteus: {day.main.humidity} %
                        <br />
                        Ilmanpaine: {day.main.pressure} hPa
                      </p>
                    ))}
                  </div>
                </div>
              </HalfContentRow>
            ))
          : null}
      </ContentWrapper>
    </>
  );
};

export default App;

const DateFormat = (datestring) => {
  const spaceSplit = datestring.split(" ");
  const year = spaceSplit[0].split("-")[0];
  const month = spaceSplit[0].split("-")[1];
  const day = spaceSplit[0].split("-")[2];
  const hour = spaceSplit[1].split(":")[0];
  const minute = spaceSplit[1].split(":")[1];
  return `${day}.${month}.${year} ${hour}:${minute}`;
};
