import axios from 'axios';
import { Basic } from '../clientheaders';

axios.defaults.baseURL = '/api';
axios.defaults.headers = Basic;


export function getWeatherAndForecast(city) {
  return axios.post('/getweatherandforecast', { city } )
    .then(response => response)
    .catch(error => error.response);
}


