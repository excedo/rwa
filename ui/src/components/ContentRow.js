import styled from 'styled-components';

const ContentRow = styled.div`
    height: auto;
    width: 100%;
    margin-top: 10px
    box-sizing: border-box;
    flex-wrap: wrap;
    display: flex;
    align-items: center;
    justify-content: center;
`;


export default ContentRow;
