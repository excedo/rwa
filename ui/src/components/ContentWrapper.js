import styled from 'styled-components';

const ContentWrapper = styled.div`
    height: auto;
    max-height: 800px;
    width: 95%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 100px
    padding-bottom: 100px;
    box-sizing: border-box;
    flex-wrap: wrap;
    display: flex;
    align-items: center;
    justify-content: center;
    overflow-y: auto;
    -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
`;


export default ContentWrapper;
