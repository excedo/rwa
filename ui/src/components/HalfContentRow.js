import styled from 'styled-components';

const HalfContentRow = styled.div`
    height: auto;
    width: 12%;
    margin-top: 10px
    box-sizing: border-box;
    display: flex;
    align-self: flex-start;
    justify-content: center;
`;


export default HalfContentRow;
