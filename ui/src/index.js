import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
    width: 100vw;
    overflow: hidden !important;
    margin: 0;
    padding: 0;
    box-sizing: border-box;

    a {
        color: #000;
        text-decoration: none;
    }
  
    a:hover 
    {
        color:#000; 
        text-decoration:none; 
        cursor:pointer;  
    }
  }
`;

ReactDOM.render(
  <>
    <GlobalStyle />
    <App />
  </>,
  document.getElementById("root")
);
